#!/bin/bash

echo Uploading Application container
docker-compose up -d

echo Copying the configuration example file
cp .env.example .env

echo -n "Enter the database name:" ''
read db

echo -n "Enter the database username:" ''
read user

echo -n "Enter the password for user '"$user"':" ''
read pass

echo -n "Enter the database root password:" ''
read root

sed -i -e "s/\(DB_USERNAME=\).*/\1$user/" \
-e "s/\(DB_PASSWORD=\).*/\1$pass/" \
-e "s/\(DB_ROOT_PASSWORD=\).*/\1$root/" \
-e "s/\(DB_DATABASE=\).*/\1$db/" ./.env

echo Install composer dependencies
docker-compose exec api composer install

echo Generate key
docker-compose exec api php artisan key:generate

echo Generate jwt key
docker-compose exec api php artisan jwt:secret

echo Run migrations
docker-compose exec api php artisan migrate

echo Run seeds
docker-compose exec api php artisan db:seed

echo Install npm dependencies
npm install

echo Build front
npm run prod

echo Show information of new containers
docker ps -a

echo Application is ready on http://localhost