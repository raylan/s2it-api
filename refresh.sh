#!/bin/bash

echo Run migrations
docker-compose exec api php artisan migrate:fresh

echo Run seeds
docker-compose exec api php artisan db:seed

echo Clear cache
docker-compose exec api php artisan cache:clear
docker-compose exec api php artisan config:clear

echo Dumpautoload
docker-compose exec api composer dumpautoload

echo Application is ready on http://localhost