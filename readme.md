## S2IT API

#### Prerequisites:

- Git
- Mysql
- Docker
- Dokcker Compose
- Node.js
- Npm

#### Instructions

- Create a database with the name of your preference

- Open your terminal and type the following commands:

  - git clone https://gitlab.com/raylan/s2it-api.git s2it-api
  - cd s2it-api
  - sh start.sh

- Type all the data that is requested in the terminal
- Override the JWT secret
- Finished!

#### Additional Information

- Use the command "sh refresh.sh" for reset all data in you database
- Use the command "docker-compose down" for turn off the containers
- Use the command "docker-compose up -d" for turn on the containers