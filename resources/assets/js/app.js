import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
Vue.use(VueAxios, axios);

Vue.component('pagination', require('laravel-vue-pagination'));

import App from './App.vue';
import Upload from './components/Upload.vue';

const routes = [
    {
        name: 'Upload',
        path: '/',
        component: Upload
    }
];

const router = new VueRouter({ mode: 'history', routes: routes});
new Vue(Vue.util.extend({ router }, App)).$mount('#app');