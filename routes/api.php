<?php

Route::post('upload', 'UploadController@upload')->name('upload');

Route::post('auth/login', 'AuthController@login')->name('auth.login');

Route::group(['middleware' => ['jwt.auth']], function () {

    Route::get('auth/logout', 'AuthController@logout')->name('auth.logout');

    Route::get('persons', 'PersonsController@list')->name('persons.list');
    Route::get('persons/{person_id}', 'PersonsController@show')->name('persons.show');

    Route::get('persons/{person_id}/phones', 'PersonPhonesController@list')->name('phones.list');
    Route::get('persons/{person_id}/phones/{phone_id}', 'PersonPhonesController@show')->name('phones.show');

    Route::get('orders', 'OrdersController@list')->name('orders.list');
    Route::get('orders/{order_id}', 'OrdersController@show')->name('orders.show');

    Route::get('orders/{order_id}/items', 'OrderItemsController@list')->name('items.list');
    Route::get('orders/{order_id}/items/{item_id}', 'OrderItemsController@show')->name('items.show');

    Route::get('orders/{order_id}/ship', 'OrderShipController@show')->name('ship.show');

});
