<?php

use Faker\Generator as Faker;
use App\Models\Order;

$factory->define(App\Models\OrderItem::class, function (Faker $faker) {
    return [
        'title'     => $faker->word,
        'note'      => $faker->text(50),
        'quantity'  => $faker->randomNumber(1),
        'price'     => $faker->randomNumber(2),
        'order_id' => Order::all()->random()->id,
    ];
});
