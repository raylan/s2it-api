<?php

use Faker\Generator as Faker;
use App\Models\Person;

$factory->define(App\Models\PersonPhone::class, function (Faker $faker) {
    return [
        'person_id' => Person::all()->random()->id,
        'phone'     => $faker->phoneNumber,
    ];
});
