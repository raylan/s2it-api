<?php

use Faker\Generator as Faker;
use App\Models\Order;

$factory->define(App\Models\OrderShip::class, function (Faker $faker) {
    return [
        'name'      => $faker->name,
        'address'   => $faker->address,
        'city'      => $faker->city,
        'country'   => $faker->country,
        'order_id'  => Order::all()->random()->id,
    ];
});