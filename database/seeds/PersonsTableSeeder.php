<?php

use Illuminate\Database\Seeder;
use App\Models\Person;

class PersonsTableSeeder extends Seeder
{
    public function run()
    {
        factory(Person::class, 10)->create();

        $this->command->info('Fake persons successfully created!');
    }
}
