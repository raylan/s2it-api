<?php

use Illuminate\Database\Seeder;
use App\Models\Order;

class OrdersTableSeeder extends Seeder
{
    public function run()
    {
        factory(Order::class, 5)->create();

        $this->command->info('Fake orders successfully created!');
    }
}
