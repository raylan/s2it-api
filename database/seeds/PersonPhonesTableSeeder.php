<?php

use Illuminate\Database\Seeder;
use App\Models\PersonPhone;

class PersonPhonesTableSeeder extends Seeder
{
    public function run()
    {
        factory(PersonPhone::class, 20)->create();

        $this->command->info('Fake phones successfully created!');
    }
}
