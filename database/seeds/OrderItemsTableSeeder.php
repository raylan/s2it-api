<?php

use Illuminate\Database\Seeder;
use App\Models\OrderItem;

class OrderItemsTableSeeder extends Seeder
{
    public function run()
    {
        factory(OrderItem::class, 25)->create();

        $this->command->info('Fake items successfully created!');
    }
}
