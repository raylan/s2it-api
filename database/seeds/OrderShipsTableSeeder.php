<?php

use Illuminate\Database\Seeder;
use App\Models\OrderShip;

class OrderShipsTableSeeder extends Seeder
{
    public function run()
    {
        factory(OrderShip::class, 5)->create();

        $this->command->info('Fake items successfully created!');
    }
}
