<?php

namespace App\Interfaces;


interface UploadInterface
{
    public function process($file);
    public function createPerson($data);
    public function createOrder($data);
}