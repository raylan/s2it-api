<?php

namespace App\Interfaces;


interface PersonInterface
{
    public function check($id);
    public function getAll();
    public function getById($id);
    public function create(array $data);
}