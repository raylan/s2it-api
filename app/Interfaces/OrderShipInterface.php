<?php

namespace App\Interfaces;


interface OrderShipInterface
{
    public function getByOrder($order);
    public function create(array $data);
}