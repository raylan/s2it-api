<?php

namespace App\Interfaces;


interface PersonPhoneInterface
{
    public function check($person, $id);
    public function getAll($person);
    public function getById($person, $id);
    public function create(array $data);
}