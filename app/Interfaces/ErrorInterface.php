<?php

namespace App\Interfaces;


interface ErrorInterface
{
    public function handleWithError($error, $status);
}