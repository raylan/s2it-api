<?php

namespace App\Interfaces;


interface OrderItemInterface
{
    public function check($order, $id);
    public function getAll($order);
    public function getById($order, $id);
    public function create(array $data);
}