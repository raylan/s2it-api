<?php

namespace App\Services;

use App\Interfaces\ErrorInterface;

class ErrorService implements ErrorInterface
{
    public function handleWithError($message, $status, $error = true)
    {
        return response()->json([
            'error' => $error,
            'message' => $message
        ], $status);
    }
}