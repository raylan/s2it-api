<?php

namespace App\Services;

use App\Models\OrderItem;
use App\Interfaces\OrderItemInterface;

class OrderItemService implements OrderItemInterface
{
    protected $orderItem;
    protected $orderService;
    protected $errorService;

    function __construct(OrderItem $orderItem, OrderService $orderService, ErrorService $errorService)
    {
        $this->orderItem = $orderItem;
        $this->orderService = $orderService;
        $this->errorService = $errorService;
    }

    public function check($order, $id)
    {
        $orderItem = $this->orderItem->where(['order_id' => $order, 'id' => $id])->first();
        if (!$orderItem) return false;
        return true;
    }

    public function getAll($order)
    {
        try {

            if (!$this->orderService->check($order))
                return $this->errorService->handleWithError('Order not Found.', 404);

            $orderItems = $this->orderItem->where('order_id', $order)
                ->select('id', 'title', 'quantity', 'price')
                ->get();

            return response()->json([
                'error' => false,
                'order_items' => $orderItems
            ]);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function getById($order, $id)
    {
        try {

            if (!$this->orderService->check($order))
                return $this->errorService->handleWithError('Order not Found.', 404);

            if (!$this->check($order, $id))
                return $this->errorService->handleWithError('Item not Found.', 404);

            $orderItem = $this->orderItem->select('id', 'title', 'quantity', 'price')
                ->find($id);

            return response()->json([
                'error' => false,
                'order_item' => $orderItem
            ]);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function create(array $data)
    {
        try {

            $item = OrderItem::updateOrCreate(
                [
                    'order_id' => $data['order_id'],
                    'title' => $data['title'],
                ],
                [
                    'note' => $data['note'],
                    'quantity' => $data['quantity'],
                    'price' => $data['price']
                ]
            );

            if (!$item) return false;

            return true;

        } catch (\Exception $e) {
            return false;
        }
    }
}