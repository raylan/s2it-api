<?php

namespace App\Services;

use App\Interfaces\AuthInterface;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthService implements AuthInterface
{
    protected $errorService;

    function __construct(ErrorService $errorService)
    {
        $this->errorService = $errorService;
    }

    public function login(array $data)
    {
        try {

            if (!$token = JWTAuth::attempt($data))
                return $this->errorService->handleWithError('Invalid credentials.', 401);

        } catch (JWTException $e) {
            return $this->errorService->handleWithError('Failed to login, please try again.', 500);
        }

        return response()->json([
            'error' => false,
            'token'=> $token
        ]);
    }

    public function logout($token)
    {
        try {

            JWTAuth::invalidate($token);

            return response()->json([
                'error' => false,
                'message'=> "You have successfully logged out."
            ]);

        } catch (JWTException $e) {
            return $this->errorService->handleWithError('Failed to logout, please try again.', 500);
        }
    }

}