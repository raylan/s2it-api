<?php

namespace App\Services;

use App\Models\PersonPhone;
use App\Interfaces\PersonPhoneInterface;

class PersonPhoneService implements PersonPhoneInterface
{
    protected $personPhone;
    protected $personService;
    protected $errorService;

    function __construct(PersonPhone $personPhone, PersonService $personService, ErrorService $errorService)
    {
        $this->personPhone = $personPhone;
        $this->personService = $personService;
        $this->errorService = $errorService;
    }

    public function check($person, $id)
    {
        $personPhone = $this->personPhone->where(['person_id' => $person, 'id' => $id])->first();
        if (!$personPhone) return false;
        return true;
    }

    public function getAll($person)
    {
        try {

            if (!$this->personService->check($person))
                return $this->errorService->handleWithError('Person not Found.', 404);

            $personPhones = $this->personPhone->where('person_id', $person)
                ->select('id', 'phone')
                ->get();

            return response()->json([
                'error' => false,
                'person_phones' => $personPhones
            ]);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function getById($person, $id)
    {
        try {

            if (!$this->personService->check($person))
                return $this->errorService->handleWithError('Person not Found.', 404);

            if (!$this->check($person, $id))
                return $this->errorService->handleWithError('Phone not Found.', 404);

            $personPhone = $this->personPhone->select('id', 'phone')
                ->find($id);

            return response()->json([
                'error' => false,
                'person_phone' => $personPhone
            ]);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function create(array $data)
    {
        try {

            $person = PersonPhone::updateOrCreate(
                [
                    'person_id' => $data['person_id'],
                    'phone' => $data['phone']
                ]
            );

            if (!$person) return false;

            return true;

        } catch (\Exception $e) {
            return false;
        }
    }
}