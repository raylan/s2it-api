<?php

namespace App\Services;

use App\Interfaces\OrderInterface;
use App\Models\Order;

class OrderService implements OrderInterface
{
    protected $order;
    protected $errorService;

    function __construct(Order $order, ErrorService $errorService)
    {
        $this->order = $order;
        $this->errorService = $errorService;
    }

    public function check($id)
    {
        $order = $this->order->find($id);
        if (!$order) return false;
        return true;
    }

    public function getAll()
    {
        try {

            $orders = $this->order->with('items:id,order_id,title,note,price', 'ship:id,order_id,name,address,city,country')
                ->select('id', 'person_id')
                ->get();

            return response()->json([
                'error' => false,
                'orders' => $orders
            ], 200);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function getById($id)
    {
        try {

            if (!$this->check($id))
                return $this->errorService->handleWithError('Order not Found.', 404);

            $order = $this->order->with('items:id,order_id,title,note,price', 'ship:id,order_id,name,address,city,country')
                ->select('id', 'person_id')
                ->find($id);

            return response()->json([
                'error' => false,
                'order' => $order
            ], 200);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function create(array $data)
    {
        try {

            $order = Order::updateOrCreate(
                [
                    'id' => $data['id']
                ],
                [
                    'person_id' => $data['person_id']
                ]
            );

            if (!$order) return false;

            return true;

        } catch (\Exception $e) {
            return false;
        }
    }
}