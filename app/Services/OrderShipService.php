<?php

namespace App\Services;

use App\Interfaces\OrderShipInterface;
use App\Models\OrderShip;

class OrderShipService implements OrderShipInterface
{
    protected $orderShip;
    protected $orderService;
    protected $errorService;

    function __construct(OrderShip $orderShip, OrderService $orderService, ErrorService $errorService)
    {
        $this->orderShip = $orderShip;
        $this->orderService = $orderService;
        $this->errorService = $errorService;
    }

    public function getByOrder($order)
    {
        try {

            if (!$this->orderService->check($order))
                return $this->errorService->handleWithError('Order not Found.', 404);

            $orderShip = $this->orderShip->where('order_id', $order)
                ->select('id', 'name', 'address', 'city', 'country', 'order_id')
                ->first();

            return response()->json([
                'error' => false,
                'order_ship' => $orderShip
            ]);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function create(array $data)
    {
        try {

            $ship = OrderShip::updateOrCreate(
                [
                    'order_id' => $data['order_id']
                ],
                [
                    'name' => $data['name'],
                    'address' => $data['address'],
                    'city' => $data['city'],
                    'country' => $data['country'],
                ]
            );

            if (!$ship) return false;

            return true;

        } catch (\Exception $e) {
            return false;
        }
    }

}