<?php

namespace App\Services;

use App\Interfaces\UploadInterface;

class UploadService implements UploadInterface
{
    private $orderService;
    private $personService;
    private $errorService;
    private $personPhoneService;
    private $orderItemService;
    private $orderShipService;

    function __construct(
        PersonService $personService,
        PersonPhoneService $personPhoneService,
        OrderItemService $orderItemService,
        OrderShipService $orderShipService,
        OrderService $orderService,
        ErrorService $errorService)
    {
        $this->orderService = $orderService;
        $this->personService = $personService;
        $this->errorService = $errorService;
        $this->personPhoneService = $personPhoneService;
        $this->orderItemService = $orderItemService;
        $this->orderShipService = $orderShipService;
    }

    public function process($request)
    {
        try {
            $file = $request->file->getRealPath();
            $data = simplexml_load_file($file);

            if ($data->person)
                return $this->createPerson($data);
            elseif ($data->shiporder)
                return $this->createOrder($data);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500, 'Wrong XML');
        }
    }

    public function createPerson($data)
    {
        try {
            foreach ($data->person as $person) {
                $this->personService->create([
                    'id' => $person->personid,
                    'name' => $person->personname
                ]);
                foreach ($person->phones->phone as $phone) {
                    $this->personPhoneService->create([
                        'person_id' => $person->personid,
                        'phone' => $phone
                    ]);
                }
            }

            return response()->json([
                'error' => false,
                'message' => 'Persons successfully uploaded!'
            ], 200);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function createOrder($data)
    {
        try {
            foreach ($data->shiporder as $shiporder) {

                $this->orderService->create([
                    'id' => $shiporder->orderid,
                    'person_id' => $shiporder->orderperson
                ]);

                $this->orderShipService->create([
                    'order_id' => $shiporder->orderid,
                    'name' => $shiporder->shipto->name,
                    'address' => $shiporder->shipto->address,
                    'city' => $shiporder->shipto->city,
                    'country' => $shiporder->shipto->country,
                ]);

                foreach ($shiporder->items->item as $item) {
                    $this->orderItemService->create([
                        'order_id' => $shiporder->orderid,
                        'title' => $item->title,
                        'note' => $item->note,
                        'quantity' => $item->quantity,
                        'price' => $item->price
                    ]);
                }
            }

            return response()->json([
                'error' => false,
                'message' => 'Orders successfully uploaded!'
            ], 200);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }
}