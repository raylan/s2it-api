<?php

namespace App\Services;

use App\Interfaces\PersonInterface;
use App\Models\Person;

class PersonService implements PersonInterface
{
    protected $person;
    protected $errorService;

    function __construct(Person $person, ErrorService $errorService)
    {
        $this->person = $person;
        $this->errorService = $errorService;
    }

    public function check($id)
    {
        $person = $this->person->find($id);
        if (!$person) return false;
        return true;
    }

    public function getAll()
    {
        try {

            $persons = $this->person->with('phones:id,person_id,phone')
                ->select('id', 'name')
                ->get();

            return response()->json([
                'error' => false,
                'persons' => $persons
            ], 200);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function getById($id)
    {
        try {

            if (!$this->check($id))
                return $this->errorService->handleWithError('Person not Found.', 404);

            $person = $this->person->with('phones:id,person_id,phone')
                ->select('id', 'name')
                ->find($id);

            return response()->json([
                'error' => false,
                'person' => $person
            ], 200);

        } catch (\Exception $e) {
            return $this->errorService->handleWithError($e->getMessage(), 500);
        }
    }

    public function create(array $data)
    {
        try {

            $person = Person::updateOrCreate(
                ['id' => $data['id']],
                ['name' => $data['name']]
            );

            if (!$person) return false;

            return true;

        } catch (\Exception $e) {
            return false;
        }
    }
}