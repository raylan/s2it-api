<?php

namespace App\Http\Controllers;

use App\Services\OrderItemService;

class OrderItemsController extends Controller
{
    protected $orderItemService;

    public function __construct(OrderItemService $orderItemService)
    {
        $this->orderItemService = $orderItemService;
    }

    public function list($order)
    {
        return $this->orderItemService->getAll($order);
    }

    public function show($order, $id)
    {
        return $this->orderItemService->getById($order, $id);
    }
}
