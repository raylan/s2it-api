<?php

namespace App\Http\Controllers;

use App\Services\PersonService;

class PersonsController extends Controller
{
    protected $personService;

    public function __construct(PersonService $personService)
    {
        $this->personService = $personService;
    }

    public function list()
    {
        return $this->personService->getAll();
    }

    public function show($id)
    {
        return $this->personService->getById($id);
    }
}
