<?php

namespace App\Http\Controllers;

use App\Services\PersonPhoneService;

class PersonPhonesController extends Controller
{
    protected $personPhoneService;

    public function __construct(PersonPhoneService $personPhoneService)
    {
        $this->personPhoneService = $personPhoneService;
    }

    public function list($person)
    {
        return $this->personPhoneService->getAll($person);
    }

    public function show($person, $id)
    {
        return $this->personPhoneService->getById($person, $id);
    }
}
