<?php

namespace App\Http\Controllers;

use App\Services\OrderService;

class OrdersController extends Controller
{
    protected $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function list()
    {
        return $this->orderService->getAll();
    }

    public function show($id)
    {
        return $this->orderService->getById($id);
    }
}
