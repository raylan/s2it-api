<?php

namespace App\Http\Controllers;

use App\Services\OrderShipService;

class OrderShipController extends Controller
{
    protected $orderShipService;

    public function __construct(OrderShipService $orderShipService)
    {
        $this->orderShipService = $orderShipService;
    }

    public function show($order)
    {
        return $this->orderShipService->getByOrder($order);
    }
}
