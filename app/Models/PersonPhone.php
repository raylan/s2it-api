<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PersonPhone extends Model
{
    use SoftDeletes;

    protected $fillable     = ['phone', 'person_id'];

    public function person()
    {
        return $this->belongsTo(Person::class);
    }
}
