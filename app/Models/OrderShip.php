<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderShip extends Model
{
    use SoftDeletes;

    protected $fillable     = ['name', 'address', 'city', 'country', 'order_id'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
