<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderItem extends Model
{
    use SoftDeletes;

    protected $fillable     = ['title', 'note', 'quantity', 'price', 'order_id'];

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
